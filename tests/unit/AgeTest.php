<?php

use app\models\User;

class AgeTest extends \Codeception\Test\Unit
{
    public function testValidation()
    {
        $user = new User();

        $user->personal_code = 37807222741;
        $this->assertTrue($user->getApplyPermission());

        $user->personal_code = 51507300248;
        $this->assertFalse($user->getApplyPermission());

        $user->personal_code = 61010160234;
        $this->assertFalse($user->getApplyPermission());

        $user->personal_code = 49304254911;
        $this->assertTrue($user->getApplyPermission());
    }
}