<?php

use yii\db\Migration;

/**
 * Class m190529_111642_alter_loan_table
 */
class m190529_111642_alter_loan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-loan-user_id',
            'loan',
            'user_id'
        );

        $this->addForeignKey(
            'fk-loan-user_id',
            'loan',
            'user_id',
            'user',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-loan-user_id', 'loan');
        $this->dropForeignKey('fk-loan-user_id', 'loan');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190529_111642_alter_loan_table cannot be reverted.\n";

        return false;
    }
    */
}
