<?php

namespace app\commands;

use app\models\Loan;
use app\models\User;
use yii\console\Controller;

class ImportController extends Controller
{
    public function actionStartImport()
    {
        $users_json = $json = file_get_contents('users.json');
        $loans_json = $json = file_get_contents('loans.json');

        $this->importUsers($users_json);
        $this->importLoans($loans_json);

        echo "Import operation completed.";
    }

    private function importLoans($loans_json)
    {
        $loans = json_decode($loans_json);

        foreach ($loans as $loan) {
            try
            {
                $loanModel = new Loan();
                $loanModel->id = $loan->id;
                $loanModel->user_id = $loan->user_id;
                $loanModel->amount = $loan->amount;
                $loanModel->interest = $loan->interest;
                $loanModel->duration = $loan->duration;
                $loanModel->start_date = date('Y-m-d', $loan->start_date);
                $loanModel->end_date = date('Y-m-d', $loan->end_date);
                $loanModel->campaign = $loan->campaign;
                $loanModel->status = $loan->status;

                if ($loanModel->validate() && $loanModel->save())
                {
                    continue;
                }
                else
                {
                    echo "Error occured. Loan #" . $loan->id . '\n';
                    echo json_encode($loanModel->errors) . '\n\n';
                }
            }
            catch (\Exception $e)
            {
                echo "Exception thrown. Message: " . $e->getMessage() . '\n';
            }
        }

        return true;
    }

    private function importUsers($users_json)
    {
        $users = json_decode($users_json);

        foreach ($users as $user) {
            try
            {
                $userModel = new User();
                $userModel->id = $user->id;
                $userModel->first_name = $user->first_name;
                $userModel->last_name = $user->last_name;
                $userModel->email = $user->email;
                $userModel->phone = $user->phone;
                $userModel->personal_code = $user->personal_code;
                $userModel->email = $user->email;
                $userModel->active = $user->active;
                $userModel->dead = $user->dead;
                $userModel->lang = $user->lang;

                if ($userModel->validate() && $userModel->save())
                {
                    continue;
                }
                else
                {
                    echo "Error occured. User #" . $user->id . '\n';
                    echo json_encode($userModel->errors) . '\n\n';
                }
            }
            catch (\Exception $e)
            {
                echo "Exception thrown. Message: " . $e->getMessage() . '\n';
            }
        }

        return true;
    }

    public function actionRemoveAll()
    {
        $loans = Loan::find()->all();
        foreach ($loans as $loan)
        {
            $loan->delete();
        }
        $users = User::find()->all();
        foreach ($users as $user)
        {
            $user->delete();
        }

        return true;
    }
}