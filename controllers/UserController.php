<?php

namespace app\controllers;

use app\models\Loan;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\data\Pagination;
use yii\data\Sort;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $sort = new Sort([
            'attributes' => [
                'id',
                'first_name',
                'last_name',
                'personal_code',
                'phone',
                'email',
                'lang'
            ],
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $countQuery = clone $dataProvider->query;

        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->defaultPageSize = 25;

        $users = $dataProvider->query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy($sort->orders)
            ->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'users' => $users,
            'pages' => $pages,
            'sort' => $sort
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $user = $this->findModel($id);

        if ($user->getAge() >= 18)
        {
            Yii::$app->session->setFlash('success', 'User is allowed to apply for a loan.');
        }
        else {
            Yii::$app->session->setFlash('error', 'User is not allowed to apply for a loan!');
        }
        return $this->render('view', [
            'model' => $user,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $loans = Loan::findAll(['user_id' => $id]);
        if ($loans != null)
        {
            Yii::$app->session->setFlash('error', 'You cannot delete a user with loans! Delete loans first.');
            return $this->redirect(['view', 'id' => $id]);
        }
        else
        {
            $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
