<?php

namespace app\models;

use Faker\Provider\DateTime;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool $active
 * @property bool $dead
 * @property string $lang
 *
 * @property Loan[] $loans
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['personal_code', 'phone'], 'default', 'value' => null],
            ['personal_code', 'string', 'length' => 11],
            [['personal_code', 'phone'], 'integer'],
            //['email', 'email'],
            [['active', 'dead'], 'boolean'],
        ];
    }

    public function getNameSurname()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['user_id' => 'id']);
    }

    public function getApplyPermission()
    {
        if ($this->getAge() >= 18)
        {
            return true;
        }

        return false;
    }

    public function getAge()
    {
       return self::calculateAge($this->personal_code);
    }

    public static function calculateAge($personal_code)
    {
        switch (substr($personal_code, 0, 1))
        {
            case 1:
            case 2:
                $x = 18;
                break;
            case 3:
            case 4:
                $x = 19;
                break;
            case 5:
            case 6:
                $x = 20;
                break;
        }
        $year = substr($personal_code, 1, 2);
        $month = substr($personal_code, 3, 2);
        $day = substr($personal_code, 5, 2);

        $birth_day = new \DateTime("$x$year-$month-$day");
        $diff = $birth_day->diff(new \DateTime("today"));

        return $diff->y;
    }
}
