#Test exercise for Creditstar Group

This exercise was completed by Ender Can Demiralp.

Contact: endercan_demiralp@yahoo.com

## Setup

Install Docker https://www.docker.com/get-started

Update your vendor packages

    docker-compose run --rm php composer update --prefer-dist
    
Run the installation triggers (creating cookie validation code)

    docker-compose run --rm php composer install    
    
Start the container

    docker-compose up -d
    
Run database migration (creating tables)

    docker-compose run --rm php yii migrate    
    docker-compose run --rm php tests/bin/yii migrate    
        
You can then access the application through the following URL:

    http://127.0.0.1:8000


Run this command to import users and loans from Json files:

        docker-compose run --rm php yii import/start-import
        
Run this command to truncate tables:

        docker-compose run --rm php yii import/remove-all

Run this command to execute tests:

        docker-compose run --rm php codecept run