<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'email')
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'email',
                'removeMaskOnSubmit' => true,
            ]
        ]);
    ?>

    <?= $form->field($model, 'personal_code')
        ->widget(MaskedInput::classname(), [
            'mask' => '99999999999',
            'clientOptions' => [
                'removeMaskOnSubmit' => true,
            ]
        ]);
    ?>

    <?= $form->field($model, 'phone')
        ->widget(MaskedInput::classname(), [
            'mask' => '(999) 999 99 99',
            'clientOptions' => [
                'removeMaskOnSubmit' => true,
            ]
        ]);
    ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'dead')->checkbox() ?>

    <?= $form->field($model, 'lang') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
