<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::button('Filter Users',
            ['class' => 'btn btn-warning', 'data-toggle' => 'modal', 'data-target' => '#user-filter-modal']) ?>
    </p>

    <table class="table">
        <thead>
        <tr>
            <th><?= $sort->link('id') ?></th>
            <th><?= $sort->link('first_name') ?></th>
            <th><?= $sort->link('last_name') ?></th>
            <th><?= $sort->link('personal_code') ?></th>
            <th><?= $sort->link('phone') ?></th>
            <th><?= $sort->link('email') ?></th>
            <th><?= $sort->link('lang') ?></th>
            <th>View</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
        <tr>
            <td><?= $user->id ?></td>
            <td><?= $user->first_name ?></td>
            <td><?= $user->last_name ?></td>
            <td><?= $user->personal_code ?></td>
            <td><?= $user->phone ?></td>
            <td><?= $user->email ?></td>
            <td><?= $user->lang ?></td>
            <td><a href="<?= Url::to(['user/view', 'id' => $user->id]) ?>" class="btn btn-sm btn-primary">Details</a></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?= LinkPager::widget(['pagination' => $pages]); ?>
</div>

<div id="user-filter-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Filter Users</h4>
            </div>
            <div class="modal-body">
                <?= $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

    </div>
</div>
