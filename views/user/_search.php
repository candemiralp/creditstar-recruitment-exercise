<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->field($model, 'personal_code')
        ->widget(MaskedInput::classname(), [
            'mask' => '99999999999',
            'clientOptions' => [
                'removeMaskOnSubmit' => true,
            ]
        ]);
    ?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'id')
                ->widget(MaskedInput::classname(), [
                    'clientOptions' => [
                        'alias' =>  'integer',
                    ],
                ]);
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'last_name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'phone')
                ->widget(MaskedInput::classname(), [
                    'mask' => '(999) 999 99 99',
                    'clientOptions' => [
                        'removeMaskOnSubmit' => true,
                    ]
                ]);
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'email') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
