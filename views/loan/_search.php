<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\LoanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation' => false,
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'user_id')->textInput()
                ->widget(MaskedInput::classname(), [
                    'clientOptions' => [
                        'alias' =>  'integer',
                    ],
                ]);
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'last_name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'amount')
                ->textInput()
                ->widget(MaskedInput::classname(), [
                    'clientOptions' => [
                        'alias' =>  'decimal',
                        'groupSeparator' => ',',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,
                    ],
                ]);
            ;?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'interest')->textInput()
                ->widget(MaskedInput::classname(), [
                    'clientOptions' => [
                        'alias' =>  'decimal',
                    ],
                ]);
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'duration')->textInput()
                ->widget(MaskedInput::classname(), [
                    'clientOptions' => [
                        'alias' =>  'integer',
                    ],
                ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'start_date')->textInput()
                ->widget(MaskedInput::classname(), [
                    'clientOptions' => [
                        'alias' =>  'yyyy-mm-dd'
                    ],
                ])
            ;?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'end_date')->textInput()
                ->widget(MaskedInput::classname(), [
                    'clientOptions' => [
                        'alias' =>  'yyyy-mm-dd'
                    ],
                ])
            ;?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
