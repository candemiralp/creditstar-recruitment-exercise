<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Loan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->field($model, 'user_id')->dropDownList($users) ?>

    <?= $form->field($model, 'amount')->textInput()
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'decimal',
                'groupSeparator' => ',',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,
            ],
        ]);
    ?>

    <?= $form->field($model, 'interest')->textInput()
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'decimal',
            ],
        ]);
    ?>

    <?= $form->field($model, 'duration')->textInput()
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
        ]);
    ?>

    <?= $form->field($model, 'start_date')->textInput()
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'yyyy-mm-dd'
            ],
        ]);?>

    <?= $form->field($model, 'end_date')->textInput()
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'yyyy-mm-dd'
            ],
        ]);?>

    <?= $form->field($model, 'campaign')->textInput()
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
        ]);
    ?>

    <?= $form->field($model, 'status')
        ->widget(MaskedInput::classname(), [
            'clientOptions' => [
                'alias' =>  'integer',
            ],
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
