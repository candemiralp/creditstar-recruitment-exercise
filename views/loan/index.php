<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Loans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Loan', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::button('Filter Loans',
            ['class' => 'btn btn-warning', 'data-toggle' => 'modal', 'data-target' => '#loan-filter-modal']) ?>
    </p>

    <table class="table">
        <thead>
        <tr>
            <th><?= $sort->link('id') ?></th>
            <th><?= $sort->link('first_name') ?></th>
            <th><?= $sort->link('last_name') ?></th>
            <th><?= $sort->link('amount') ?></th>
            <th><?= $sort->link('interest') ?></th>
            <th><?= $sort->link('duration') ?></th>
            <th><?= $sort->link('start_date') ?></th>
            <th><?= $sort->link('end_date') ?></th>
            <th>View</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($loans as $loan): ?>
        <tr>
            <td><?= $loan->id ?></td>
            <td><?= $loan->user->first_name ?></td>
            <td><?= $loan->user->last_name ?></td>
            <td><?= $loan->amount ?> €</td>
            <td><?= $loan->interest ?> %</td>
            <td><?= $loan->duration ?></td>
            <td><?= $loan->start_date ?></td>
            <td><?= $loan->end_date ?></td>
            <td><a href="<?= Url::to(['loan/view', 'id' => $loan->id]) ?>" class="btn btn-sm btn-primary">Details</a></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?= LinkPager::widget(['pagination' => $pages]); ?>
</div>

<div id="loan-filter-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Filter Loans</h4>
            </div>
            <div class="modal-body">
                <?= $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>

    </div>
</div>