<?php

/* @var $this yii\web\View */

$this->title = 'Credit Star Recruitment Exercise';

use yii\helpers\Url; ?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Hello World!</h1>
        <p class="lead">You have successfully created your Yii-powered application.</p>
        <p style="margin-top: 50px">
            <a class="btn btn-lg btn-success" href="<?= Url::to(['user/index']) ?>">Users</a>
            <a class="btn btn-lg btn-warning" href="<?= Url::to(['loan/index']) ?>">Loans</a>
        </p>
    </div>
</div>
